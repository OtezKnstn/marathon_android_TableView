package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView



class MainActivity : AppCompatActivity() {

    private var main_button: Button? = null
    private var pic: ImageView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        main_button = findViewById(R.id.main_button)
        pic = findViewById(R.id.pic)

        fun show_image(image: ImageView, x: Int){
            when(x){
                1 -> image.setBackgroundResource(R.drawable.perviy)
                2 -> image.setBackgroundResource(R.drawable.vtoroy)
                3 -> image.setBackgroundResource(R.drawable.tretiy)
                4 -> image.setBackgroundResource(R.drawable.chetvertiy)
                5 -> image.setBackgroundResource(R.drawable.pyatiy)
                6 -> image.setBackgroundResource(R.drawable.shestoy)

            }
        }

        main_button?.setOnClickListener(){
            val i: Int = (1..6).random()
            when(i) {
                1 -> main_button?.setBackgroundColor(getResources().getColor(R.color.teal_700))
                2 -> main_button?.setBackgroundColor(getResources().getColor(R.color.purple_500))
                else -> main_button?.setBackgroundColor(getResources().getColor(R.color.teal_200))
            }

            show_image(pic!!, i)
        }
    }
}